﻿
#include <iostream>
#include <vector>
#include <string>
#include <windows.h>
#include <typeinfo>
#include <cstdlib>
#include <fstream>
using namespace std;

class Investments
{
public:
	string KindOfInv;
	float CostOfInv;
	int AddDataInv[3];

	friend ostream& operator<<(ostream& stream, Investments& obj);
	friend istream& operator>>(istream& stream, Investments& obj);

	friend ofstream& operator<< (ofstream& stream, Investments& obj);
	friend ifstream& operator>> (ifstream& stream, Investments& obj);

	virtual void Test() = 0;

};

struct Money : public Investments
{
	float AmountOfMoney;

	Money()
	{
		this->KindOfInv = "NULL";
		this->AmountOfMoney = NULL;
		this->CostOfInv = NULL;
		for (int i = 0; i < 3; i++)
		{
			this->AddDataInv[i] = NULL;
		}
	}

	void Test()
	{}

	friend ostream& operator<<(ostream& stream, Money& obj);
	friend istream& operator>>(istream& stream, Money& obj);

	friend ofstream& operator<< (ofstream& stream, Money& obj);
	friend ifstream& operator>> (ifstream& stream, Money& obj);

};
struct SecuritiPaper : public Investments
{
	string ConpamyName;


	SecuritiPaper()
	{
		this->KindOfInv = "NULL";
		this->ConpamyName = "NULL";
		this->CostOfInv = NULL;
		for (int i = 0; i < 3; i++)
		{
			this->AddDataInv[i] = NULL;
		}



	}

	void Test()
	{}

	friend ostream& operator<<(ostream& stream, SecuritiPaper& obj);
	friend istream& operator>>(istream& stream, SecuritiPaper& obj);

	friend ofstream& operator<< (ofstream& stream, SecuritiPaper& obj);
	friend ifstream& operator>> (ifstream& stream, SecuritiPaper& obj);
};
struct ValuableMetal : public Investments
{
	ValuableMetal()
	{
		this->KindOfInv = "NULL";
		this->CostOfInv = NULL;
		for (int i = 0; i < 3; i++)
		{
			this->AddDataInv[i] = NULL;
		}
	}

	void Test()
	{}

	friend ostream& operator<<(ostream& stream, ValuableMetal& obj);
	friend istream& operator>>(istream& stream, ValuableMetal& obj);

	friend ofstream& operator<< (ofstream& stream, ValuableMetal& obj);
	friend ifstream& operator>> (ifstream& stream, ValuableMetal& obj);
};
struct Property : public Investments
{
	float SqM;

	Property()
	{
		this->KindOfInv = "NULL";
		this->SqM = NULL;
		this->CostOfInv = NULL;
		for (int i = 0; i < 3; i++)
		{
			this->AddDataInv[i] = NULL;
		}
	}

	void Test()
	{}

	friend ostream& operator<<(ostream& stream, Property& obj);
	friend istream& operator>>(istream& stream, Property& obj);

	friend ofstream& operator<< (ofstream& stream, Property& obj);
	friend ifstream& operator>> (ifstream& stream, Property& obj);
};

ofstream fout;
ifstream fin;

void fFailCheck();

class Bag
{
	string OwnerName;
	vector<Investments*> vInv;
	float TotalCost;
	float InvCost;

public:
	Bag()
	{
		this->OwnerName = "NULL";
		this->InvCost = 0;
		this->TotalCost = 0;
	}

	void GetOwnerName(string ON)
	{
		this->OwnerName = ON;
	}
	string ShowOwnerName()
	{
		return OwnerName;
	}

	void GetToatalCost(float cost)
	{
		this->TotalCost = cost;
	}
	float ShowToatalCost()
	{
		return TotalCost;
	}
	float ShowInvCost()
	{
		return InvCost;
	}
	void GetInvCost(float cost)
	{
		this->InvCost = cost;
	}

	void AddInv(int KindOfInv)
	{
		system("cls");

		switch (KindOfInv)
		{
		default:
			cout << "Инвестиция не выбрана\n";
			system("pause>nul");
			break;

		case 1:
		{
			Money* mTmp = new Money;
			cin >> *mTmp;
			vInv.push_back(mTmp);
			TotalCost += ((float)mTmp->AmountOfMoney * mTmp->CostOfInv);
			break;
		}
		case 2:
		{
			SecuritiPaper* pTmp = new SecuritiPaper();
			cin >> *pTmp;
			vInv.push_back(pTmp);

			TotalCost += pTmp->CostOfInv;
			break;
		}
		case 3:
		{
			ValuableMetal* mTmp = new ValuableMetal();
			cin >> *mTmp;
			vInv.push_back(mTmp);
			TotalCost += mTmp->CostOfInv;
			break;
		}
		case 4:
		{
			Property* pTmp = new Property();
			cin >> *pTmp;
			vInv.push_back(pTmp);
			TotalCost += pTmp->CostOfInv;
			break;
		}
		case 5:
			return;
		}
	}
	void DelInv()
	{
		system("cls");

		if (vInv.empty())
		{
			cout << "В данном портфеле отсутствуют инвестиции.\n";
			system("pause>nul");
		}
		else
		{
			int DelChoose;

			cout << "Список инвестиций:\n";
			for (int i = 0; i < vInv.size(); i++)
			{
				cout << i + 1 << "." << *(vInv[i]);
				cout << "\n";
			}
			cout << "Для отмены введите цифру: " << (vInv.size() + 1) << "\n";
			cout << "Введите номер инвестиции для удаления: ";
			cin >> DelChoose;

			fFailCheck();
			if (DelChoose == (vInv.size() + 1)) return;
			if (DelChoose > vInv.size() + 1 || DelChoose < 1)
			{
				cout << "Такой ивестиции не существует.\n";
				system("pause>nul");
				return;
			}

			vInv.erase(vInv.begin() + (DelChoose - 1));
		}
	}
	void ShowInv()
	{
		system("cls");

		if (vInv.empty())
		{
			cout << "В данном портфеле отсутствуют инвестиции.\n";
			system("pause>nul");
		}
		else
		{
			cout << "Список инвестиций:\n";
			cout << "\n";
			for (int i = 0; i < vInv.size(); i++)
			{
				cout << *(vInv[i]);
				cout << "\n";
			}
			cout << "Общая стоимость портфеля: " << this->TotalCost << " BYR" << endl;
			cout << "Имя владельца: " << this->OwnerName << endl;
			cout << endl;
			system("pause>nul");
		}
	}
	void SerchInv(int KindOfInv)
	{
		if (vInv.empty())
		{
			return;
		}
		else
		{
			for (int i = 0; i < vInv.size(); i++)
			{
				if (KindOfInv == 1)
				{
					if (typeid(*vInv[i]) == typeid(Money))
					{
						Money* tmp;
						tmp = dynamic_cast<Money*>(vInv[i]);
						cout << *vInv[i] << "Владелец инвестиции: " << OwnerName << endl << endl;
						InvCost += tmp->CostOfInv * (tmp->AmountOfMoney);
					}
				}
				if (KindOfInv == 2)
				{
					if (typeid(*vInv[i]) == typeid(SecuritiPaper))
					{
						cout << *vInv[i] << "Владелец инвестиции: " << OwnerName << endl << endl;
						InvCost += vInv[i]->CostOfInv;
					}
				}
				if (KindOfInv == 3)
				{
					if (typeid(*vInv[i]) == typeid(ValuableMetal))
					{
						cout << *vInv[i] << "Владелец инвестиции: " << OwnerName << endl << endl;
						InvCost += vInv[i]->CostOfInv;
					}
				}
				if (KindOfInv == 4)
				{
					if (typeid(*vInv[i]) == typeid(Property)) cout << *vInv[i] << "Владелец инвестиции: " << OwnerName << endl << endl;
					InvCost += vInv[i]->CostOfInv;
				}
			}
		}
	}
	void GetInv(Investments* ptr)
	{
		vInv.push_back(ptr);

	}
	void SaveInv()
	{
		if (vInv.empty())
		{
			return;
		}
		else
		{
			for (int i = 0; i < vInv.size(); i++)
			{
				if (typeid(*vInv[i]) == typeid(Money))
				{
					fout << *vInv[i];
				}
				if (typeid(*vInv[i]) == typeid(SecuritiPaper))
				{
					fout << *vInv[i];
				}
				if (typeid(*vInv[i]) == typeid(ValuableMetal))
				{
					fout << *vInv[i];
				}
				if (typeid(*vInv[i]) == typeid(Property))
				{
					fout << *vInv[i];
				}
			}
		}
	}
};

void fAddBag(vector<Bag>& fvBag);
void fDelBag(vector<Bag>& fvBag);
void fShowBag(vector<Bag>& fvBag);
void fSearchBag(vector<Bag>& fvBag);
void fSearchInvInBag(vector<Bag>& fvBag);
void fMostExpensiveBag(vector<Bag>& fvBag);
void fFileSave(vector<Bag>& fvBag);


int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	vector<Bag> vBag;

	fin.open("data.txt");
	if (fin.is_open()) //проверка на наличие файла;
	{
		char cBuffer;
		while (!fin.eof())
		{
			fin >> cBuffer;
			if (cBuffer == '{')
			{
				Bag bTmp;
				string name;
				float cost;

				fin >> cBuffer;

				if (cBuffer == '<')
				{
					getline(fin, name, '>');
					bTmp.GetOwnerName(name);
					fin >> cost;
					bTmp.GetToatalCost(cost);
				}
				else
				{
					cout << "Ошибка при заиси из файла!!!\n";
				}

				while (cBuffer != '}')
				{
					fin >> cBuffer;

					switch (cBuffer)
					{
					case '1':
					{
						Money* tmp = new Money;
						fin >> *tmp;
						bTmp.GetInv(tmp);
						break;
					}
					case '2':
					{
						SecuritiPaper* tmp = new SecuritiPaper;
						fin >> *tmp;
						bTmp.GetInv(tmp);
						break;
					}
					case '3':
					{
						ValuableMetal* tmp = new ValuableMetal;
						fin >> *tmp;
						bTmp.GetInv(tmp);
						break;
					}
					case '4':
					{
						Property* tmp = new Property;
						fin >> *tmp;
						bTmp.GetInv(tmp);
						break;
					}
					case '}':
					{
						vBag.push_back(bTmp);
						break;
					}
					}

				}
			}

		}

		fin.close();
	}
	else
	{
		fout.open("data.txt");
		fout.close();
	}

	bool EndConsl(true);
	string MenUSelect;
	int ChooseBag;
	int ChooseInv;

	//Цикл меню
	while (EndConsl)
	{
		while (true)
		{
			system("cls");
			cout << "Введите пункт меню:\n"
				<< "1. Создание инвестиционного портфеля;\n"
				<< "2. Удаление инвестиционного портфеля;\n"
				<< "3. Отображение инвестиционных портфелей;\n"
				<< "4. Добавить инвестицию в портфель;\n"
				<< "5. Удалить инвестицию в портфеле;\n"
				<< "6. Отображение инвистиций в портфеле;\n"
				<< "7. Поиск портфеля по имени владельца;\n"
				<< "8. Поиск инвестиций по типу;\n"
				<< "9. Поиск самого дорогостоящего портфеля;\n"
				<< "10. Выход;\n"
				<< "\n11. Справка.\n"
				<< ">";

			cin >> MenUSelect;

			system("cls");
			if (MenUSelect == "1")
			{
				fAddBag(vBag);

				fFileSave(vBag);
			}
			else if (MenUSelect == "2")
			{
				fDelBag(vBag);

				fFileSave(vBag);
			}
			else if (MenUSelect == "3")
			{
				fShowBag(vBag);
			}
			else if (MenUSelect == "4")
			{
				system("cls");

				if (vBag.empty())
				{
					cout << "Нет портфелей!!!" << endl;
					system("pause>nul");
					break;
				}
				else
				{
					cout << "Список инвестиционных портфелей:\n";
					for (unsigned int i = 0; i < vBag.size(); i++)
					{
						cout << i + 1 << "." << vBag[i].ShowOwnerName() << endl;
					}
				}
				cout << "Для отмены введите цифру: " << (vBag.size() + 1) << "\n";

				cout << "Введите номер портфеля в который хотите добавить инвестицию: ";
				cin >> ChooseBag;
				cout << "\n";

				fFailCheck();
				if (ChooseBag == (vBag.size() + 1)) break;
				if (ChooseBag > (vBag.size() + 1) || ChooseBag < 1)
				{
					cout << "Такого портефеля не существует.\n";
					system("pause>nul");
					break;
				}

				system("cls");

				cout << "Введите номер инвестиции для добавления:\n";
				cout << "1.Инвестиция валютная;\n"
					<< "2.Инвестиция ценных бумага;\n"
					<< "3.Инвестиция драгоценных металлов;\n"
					<< "4.Инвестиция недвижимости;\n"
					<< "5.Вернутся в главное меню;\n"
					<< ">";
				cin >> ChooseInv;
				fFailCheck();

				vBag[ChooseBag - 1].AddInv(ChooseInv);

				fFileSave(vBag);
			}
			else if (MenUSelect == "5")
			{
				if (vBag.size() == 0)
				{
					cout << "Нет портфелей!!!" << endl;
					system("pause>nul");
					break;
				}
				else
				{
					cout << "Список инвестиционных портфелей:\n";
					for (unsigned int i = 0; i < vBag.size(); i++)
					{
						cout << i + 1 << "." << vBag[i].ShowOwnerName() << endl;
					}
				}
				cout << "Для отмены введите цифру: " << (vBag.size() + 1) << "\n";

				int ChooseBag;

				cout << "Введите номер портфеля из которого хотите удалить инвестиции: ";
				cin >> ChooseBag;

				fFailCheck();
				if (ChooseBag == (vBag.size() + 1)) break;
				if (ChooseBag > (vBag.size() + 1) || ChooseBag < 1)
				{
					cout << "Такого портефеля не существует.\n";
					system("pause>nul");
					break;
				}

				vBag[ChooseBag - 1].DelInv();

				fFileSave(vBag);
			}
			else if (MenUSelect == "6")
			{
				if (vBag.size() == 0)
				{
					cout << "Нет портфелей!!!" << endl;
					system("pause>nul");
					break;
				}
				else
				{
					cout << "Список инвестиционных портфелей:\n";
					for (unsigned int i = 0; i < vBag.size(); i++)
					{
						cout << i + 1 << "." << vBag[i].ShowOwnerName() << endl;
					}
				}
				cout << "Для отмены введите цифру: " << (vBag.size() + 1) << "\n";

				cout << "Введите номер интересующего вас портфеля: ";
				cin >> ChooseBag;

				fFailCheck();
				if (ChooseBag == (vBag.size() + 1)) break;
				if (ChooseBag > (vBag.size() + 1) || ChooseBag < 1)
				{
					cout << "Такого портефеля не существует.\n";
					system("pause>nul");
					break;
				}

				vBag[(ChooseBag - 1)].ShowInv();
			}
			else if (MenUSelect == "7")
			{
				fSearchBag(vBag);
			}
			else if (MenUSelect == "8")
			{
				fSearchInvInBag(vBag);
			}
			else if (MenUSelect == "9")
			{
				fMostExpensiveBag(vBag);
			}
			else if (MenUSelect == "10")
			{
				fFileSave(vBag);
				EndConsl = false;
				break;
			}
			else if (MenUSelect == "11")
			{
				system("cls");

				cout << "Разаработчик: Матылевич Никита Алексеевич.\n";
				cout << "МГКЭ, МИНСК, 2019\n";
				system("pause");

			}
			else
			{
				cout << "Неверный пункт меню!!!\n";
				system("pause>nul");
			}
		}
	}
	return 0;
}

//Перегрузка опрераторов ввода-вывода для Investments
ostream& operator<<(ostream& stream, Investments& obj)
{
	if (typeid(obj) == typeid(Money)) stream << dynamic_cast<Money&>(obj);
	if (typeid(obj) == typeid(SecuritiPaper)) stream << dynamic_cast<SecuritiPaper&>(obj);
	if (typeid(obj) == typeid(ValuableMetal)) stream << dynamic_cast<ValuableMetal&>(obj);
	if (typeid(obj) == typeid(Property)) stream << dynamic_cast<Property&>(obj);
	return stream;
}
istream& operator>>(istream& stream, Investments& obj)
{
	if (typeid(obj) == typeid(Money)) stream >> dynamic_cast<Money&>(obj);
	if (typeid(obj) == typeid(SecuritiPaper)) stream >> dynamic_cast<SecuritiPaper&>(obj);
	if (typeid(obj) == typeid(ValuableMetal)) stream >> dynamic_cast<ValuableMetal&>(obj);
	if (typeid(obj) == typeid(Property)) stream >> dynamic_cast<Property&>(obj);
	return stream;
}
ofstream& operator<<(ofstream& stream, Investments& obj)
{
	if (typeid(obj) == typeid(Money)) stream << dynamic_cast<Money&>(obj);
	if (typeid(obj) == typeid(SecuritiPaper)) stream << dynamic_cast<SecuritiPaper&>(obj);
	if (typeid(obj) == typeid(ValuableMetal)) stream << dynamic_cast<ValuableMetal&>(obj);
	if (typeid(obj) == typeid(Property)) stream << dynamic_cast<Property&>(obj);
	return stream;
}
ifstream& operator>>(ifstream& stream, Investments& obj)
{
	if (typeid(obj) == typeid(Money)) stream >> dynamic_cast<Money&>(obj);
	if (typeid(obj) == typeid(SecuritiPaper)) stream >> dynamic_cast<SecuritiPaper&>(obj);
	if (typeid(obj) == typeid(ValuableMetal)) stream >> dynamic_cast<ValuableMetal&>(obj);
	if (typeid(obj) == typeid(Property)) stream >> dynamic_cast<Property&>(obj);
	return stream;
}
//Перегрузка опрераторов ввода-вывода для Money
ostream& operator<<(ostream& stream, Money& obj)
{
	stream << "Валюта в: " << obj.KindOfInv << "\n";
	stream << "Кол-во денег: " << obj.AmountOfMoney << " " << obj.KindOfInv << "\n";
	stream << "Cтоимость за ед. валюту: " << obj.CostOfInv << " BYR" << "\n";
	stream << "Дата добавления: " << obj.AddDataInv[0] << "." << obj.AddDataInv[1] << "." << obj.AddDataInv[2] << "\n";
	return stream;
}
istream& operator>>(istream& stream, Money& obj)
{
	cout << "Введите валюту: ";
	stream >> obj.KindOfInv;
	cout << "\n";

	cout << "Ведите кол-во денег: ";
	stream >> obj.AmountOfMoney;
	cout << "\n";

	cout << "Ведите стоимость за ед. валюты в BYR: ";
	stream >> obj.CostOfInv;
	cout << "\n";

	cout << "Введите дату добавления: ";
	stream >> obj.AddDataInv[0] >> obj.AddDataInv[1] >> obj.AddDataInv[2];
	cout << "\n";
	return stream;
}
ofstream& operator<<(ofstream& stream, Money& obj)
{
	stream << "1" << " " << obj.KindOfInv << " " << obj.AmountOfMoney << " " << obj.CostOfInv << " " << obj.AddDataInv[0] << " " << obj.AddDataInv[1] << " " << obj.AddDataInv[2] << "\n";
	return stream;
}
ifstream& operator>>(ifstream& stream, Money& obj)
{
	stream >> obj.KindOfInv >> obj.AmountOfMoney >> obj.CostOfInv >> obj.AddDataInv[0] >> obj.AddDataInv[1] >> obj.AddDataInv[2];
	return stream;
}
//Перегрузка опрераторов ввода-вывода для SecuritiPaper
ostream& operator<<(ostream& stream, SecuritiPaper& obj)
{
	stream << "Тип ценной бумаги: " << obj.KindOfInv << "\n";
	stream << "Стоимость ценной бумаги: " << obj.CostOfInv << " BYR" << "\n";
	stream << "Эмитент: " << obj.ConpamyName << "\n";
	stream << "Дата добавления: " << obj.AddDataInv[0] << "." << obj.AddDataInv[1] << "." << obj.AddDataInv[2] << "\n";
	return stream;
}
istream& operator>>(istream& stream, SecuritiPaper& obj)
{
	cout << "Введите тип ценной бумаги: ";
	stream >> obj.KindOfInv;
	cout << "\n";

	cout << "Введите стоимость ценной бумаги в BYR: ";
	stream >> obj.CostOfInv;
	cout << "\n";

	cout << "Введите название фирмы эмитента: ";
	cin.get();
	getline(stream, obj.ConpamyName);
	cout << "\n";

	cout << "Введите дату добавления: ";
	stream >> obj.AddDataInv[0] >> obj.AddDataInv[1] >> obj.AddDataInv[2];
	cout << "\n";
	return stream;
}
ofstream& operator<<(ofstream& stream, SecuritiPaper& obj)
{
	stream << "2" << " " << obj.KindOfInv << " " << "<" << obj.ConpamyName << ">" << " " << obj.CostOfInv << " " << obj.AddDataInv[0] << " " << obj.AddDataInv[1] << " " << obj.AddDataInv[2] << "\n";
	return stream;
}
ifstream& operator>>(ifstream& stream, SecuritiPaper& obj)
{
	char cBuffer;
	stream >> obj.KindOfInv;
	stream >> cBuffer;
	if (cBuffer == '<')
	{
		getline(stream, obj.ConpamyName, '>');
		stream >> obj.CostOfInv >> obj.AddDataInv[0] >> obj.AddDataInv[1] >> obj.AddDataInv[2];
	}
	else
	{
		cout << "Неправильная запись в файле.\n";
		system("pause");
	}
	return stream;
}
//Перегрузка опрераторов ввода-вывода для ValuableMetal
ostream& operator<<(ostream& stream, ValuableMetal& obj)
{
	stream << "Тип драгоценного металла: " << obj.KindOfInv << "\n";
	stream << "Стоимость драгоценного металла : " << obj.CostOfInv << " BYR" << "\n";
	stream << "Дата добавления: " << obj.AddDataInv[0] << "." << obj.AddDataInv[1] << "." << obj.AddDataInv[2] << "\n";
	return stream;
}
istream& operator>>(istream& stream, ValuableMetal& obj)

{
	cout << "Введите тип драгоценного металла: ";
	stream >> obj.KindOfInv;
	cout << "\n";

	cout << "Введите стоимость драгоценного металла в BYR: ";
	stream >> obj.CostOfInv;
	cout << "\n";

	cout << "Введите дату добавления: ";
	stream >> obj.AddDataInv[0] >> obj.AddDataInv[1] >> obj.AddDataInv[2];
	cout << "\n";
	return stream;
}
ofstream& operator<<(ofstream& stream, ValuableMetal& obj)
{
	stream << "3" << " " << obj.KindOfInv << " " << obj.CostOfInv << " " << obj.AddDataInv[0] << " " << obj.AddDataInv[1] << " " << obj.AddDataInv[2] << "\n";
	return stream;
}
ifstream& operator>>(ifstream& stream, ValuableMetal& obj)
{
	stream >> obj.KindOfInv >> obj.CostOfInv >> obj.AddDataInv[0] >> obj.AddDataInv[1] >> obj.AddDataInv[2];
	return stream;
}
//Перегрузка опрераторов ввода-вывода для Property
ostream& operator<<(ostream& stream, Property& obj)
{
	stream << "Тип недвижимости: " << obj.KindOfInv << "\n";
	stream << "Площадь в метрах квадратных: " << obj.SqM << "\n";
	stream << "Cтоимость недвижимости: " << obj.CostOfInv << " BYR" << "\n";
	stream << "Дата добавления: " << obj.AddDataInv[0] << "." << obj.AddDataInv[1] << "." << obj.AddDataInv[2] << "\n";
	return stream;
}
istream& operator>>(istream& stream, Property& obj)
{
	cout << "Введите тип недвижимости: ";
	stream >> obj.KindOfInv;
	cout << "\n";

	cout << "Ведите площадь в метрах квадратных: ";
	stream >> obj.SqM;
	cout << "\n";

	cout << "Ведите стоимость недвижимости в BYR: ";
	stream >> obj.CostOfInv;
	cout << "\n";

	cout << "Введите дату добавления: ";
	stream >> obj.AddDataInv[0] >> obj.AddDataInv[1] >> obj.AddDataInv[2];
	cout << "\n";
	return stream;
}
ofstream& operator<<(ofstream& stream, Property& obj)
{
	stream << "4" << " " << obj.KindOfInv << " " << obj.SqM << " " << obj.CostOfInv << " " << obj.AddDataInv[0] << " " << obj.AddDataInv[1] << " " << obj.AddDataInv[2] << "\n";
	return stream;
}
ifstream& operator>>(ifstream& stream, Property& obj)
{
	stream >> obj.KindOfInv >> obj.SqM >> obj.CostOfInv >> obj.AddDataInv[0] >> obj.AddDataInv[1] >> obj.AddDataInv[2];
	return stream;
}

//Функции по обработке портфелей(Bag)
void fAddBag(vector<Bag>& fvBag)
{
	string ON;
	cout << "Введите Ф.И.О владельца: ";
	cin.get();
	getline(cin, ON);
	cout << "\n";

	Bag TmpBag;
	TmpBag.GetOwnerName(ON);
	fvBag.push_back(TmpBag);
}
void fDelBag(vector<Bag>& fvBag)
{
	system("cls");

	if (fvBag.size() == 0)
	{
		cout << "Нет портфелей!!!" << endl;
		system("pause>nul");
	}
	else
	{
		int DelChoose;

		cout << "Список инвестиционных портфелей:\n";
		for (unsigned int i = 0; i < fvBag.size(); i++)
		{
			cout << i + 1 << "." << fvBag[i].ShowOwnerName() << endl;
		}
		cout << "Для отмены введите цифру: " << (fvBag.size() + 1) << "\n";

		cout << "Ведите номер портфеля для удаления: ";
		cin >> DelChoose;

		fFailCheck();
		if (DelChoose == (fvBag.size() + 1)) return;
		if (DelChoose > (fvBag.size() + 1) || DelChoose < 1)
		{
			cout << "Такого портефеля не существует.\n";
			system("pause>nul");
			return;
		}

		fvBag.erase(fvBag.begin() + (DelChoose - 1));
	}

}
void fShowBag(vector<Bag>& fvBag)
{
	if (fvBag.size() == 0)
	{
		cout << "Нет портфелей!!!" << endl;
		system("pause");
	}
	else
	{
		cout << "Список инвестиционных портфелей:\n";
		for (unsigned int i = 0; i < fvBag.size(); i++)
		{
			cout << i + 1 << "." << fvBag[i].ShowOwnerName() << endl;
			cout << "Стоимость портфеля: " << fvBag[i].ShowToatalCost() << " BYR" << endl;
			cout << endl;
		}
		system("pause>nul");
	}
}
void fSearchBag(vector<Bag>& fvBag)
{
	if (fvBag.size() == 0)
	{
		cout << "Нет портфелей!!!" << endl;
		system("pause");
	}
	else
	{
		string sName;
		cout << "Введите имя: ";
		cin.get();
		getline(cin, sName);

		for (int i = 0; i < fvBag.size(); i++)
		{
			if (fvBag[i].ShowOwnerName() == sName)
			{
				fvBag[i].ShowInv();
				system("pause>nul");
				return;
			}
		}
		cout << "Портфеля с данным владельцом не существует.";
		system("pause>nul");
	}
}
void fSearchInvInBag(vector<Bag>& fvBag)
{

	if (fvBag.size() == 0)
	{
		cout << "Нет портфелей!!!" << endl;
		system("pause>nul");
	}
	else
	{
		int ChooseInv;
		float CostInv(0);

		cout << "Введите номер инвестиции для поиска:\n";
		cout << "1.Инвестиция валютная;\n"
			<< "2.Инвестиция ценных бумага;\n"
			<< "3.Инвестиция драгоценных металлов;\n"
			<< "4.Инвестиция недвижимости;\n"
			<< "5.Вернутся в глвыное меню;\n"
			<< ">";
		cin >> ChooseInv;
		fFailCheck();
		if (ChooseInv == 5) return;
		if (ChooseInv > 5 || ChooseInv < 1)
		{
			cout << "Такого типа не существует.\n";
			system("pause>nul");
			return;
		}

		system("cls");

		cout << "Инвеситиции подходящее по типу:\n";

		for (int i = 0; i < fvBag.size(); i++)
		{
			fvBag[i].SerchInv(ChooseInv);
			CostInv += fvBag[i].ShowInvCost();
			fvBag[i].GetInvCost(0.0);
		}

		cout << "Общая стоимость данного типа инвестиций: " << CostInv << " BYR";

		system("pause>nul");
	}
}
void fMostExpensiveBag(vector<Bag>& fvBag)
{
	if (fvBag.size() == 0)
	{
		cout << "Нет портфелей!!!" << endl;
		system("pause>nul");
	}
	else
	{
		float MaxCost = fvBag[0].ShowToatalCost();
		int index(0);

		for (int i = 0; i < fvBag.size(); i++)
		{
			if (fvBag[i].ShowToatalCost() > MaxCost)
			{
				MaxCost = fvBag[i].ShowToatalCost();
				index = i;
			}
		}

		cout << "Владелец самого дорогостоящего инвестиционного портфеля: " << fvBag[index].ShowOwnerName() << "\n";
		cout << "Стоимость:" << fvBag[index].ShowToatalCost() << " BYR";
		system("pause>nul");
	}
}
//Функция для записи в файл
void fFileSave(vector<Bag>& fvBag)
{
	fout.open("data.txt");

	for (int i = 0; i < fvBag.size(); i++)
	{
		fout << "{\n";
		fout << "<" << fvBag[i].ShowOwnerName() << ">" << "\n";
		fout << fvBag[i].ShowToatalCost() << "\n";
		fvBag[i].SaveInv();
		fout << "}\n";
	}

	fout.close();
}

void fFailCheck()
{
	if (cin.fail())
	{
		cout << "Неправильный ввод!!!\n";
		cin.clear();
		cin.ignore();
	}
}