﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LvlObjectType : MonoBehaviour
{
    public LvlObjectTypes ObjectType;
}
public enum LvlObjectTypes
{
    LvlBlock,
    LvlStartBlock,
    LvlFinishBlock,
    LvlPathBlock,
    PlayerTile,
}