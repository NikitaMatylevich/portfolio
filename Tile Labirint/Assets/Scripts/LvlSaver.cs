﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
public class LvlSaver : MonoBehaviour 
{
    [SerializeField] private int rows;
    [SerializeField] private int index;
    [SerializeField] private int playerSize;

    [ContextMenu("Save lvl")]
    private void LvlSave()
    {
        LvlData lvlData = new LvlData();
        lvlData.rows = rows;
        lvlData.PlayerSize = playerSize;
        lvlData.LvlArr = new int[this.transform.childCount];

        for (int i = 0; i < this.transform.childCount; i++)
        {
            var obj = this.transform.GetChild(i);
            switch (obj.GetComponent<LvlObjectType>().ObjectType)
            {
                case LvlObjectTypes.LvlBlock:
                    lvlData.LvlArr[i] = 0;
                    break;

                case LvlObjectTypes.LvlStartBlock:
                    lvlData.LvlArr[i] = 2;
                    break;

                case LvlObjectTypes.LvlFinishBlock:
                    lvlData.LvlArr[i] = 3;
                    break;

                case LvlObjectTypes.LvlPathBlock:
                    lvlData.LvlArr[i] = 1;
                    break;

                default:
                    break;
            }
        }

        lvlData.CameraPos = FindObjectOfType<Camera>().transform.position;
        lvlData.CameraRotation = FindObjectOfType<Camera>().transform.rotation;

        string json = JsonUtility.ToJson(lvlData);

        File.WriteAllText($"Assets/Resources/Data/Levels/Level{index}.json", json);


        AssetDatabase.Refresh();


    }
}
#endif
