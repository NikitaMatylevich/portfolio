﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerInput : MonoBehaviour
{

    private Vector3 startTouchPos;
    private Vector3 endTouchPos;
    private Vector3 delta;

    public Vector3 CheckInput()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return Vector3.zero;
        }

        delta = Vector3.zero;

        if (Input.GetMouseButtonDown(0))
        {
            startTouchPos = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0) )
        {
            endTouchPos = Input.mousePosition;

            delta = endTouchPos - startTouchPos;
        }

        if (Mathf.Abs(delta.x) > Mathf.Abs(delta.y))
        {
            if (delta.x > 0)
            {
                return Vector3.left;
            }
            if (delta.x < 0)
            {
                return Vector3.right;
            }
        }
        else
        {
            if (delta.y > 0)
            {
                return Vector3.back;
            }
            if (delta.y < 0)
            {
                return Vector3.forward;
            }
        }

        return Vector3.zero;
    }
}
