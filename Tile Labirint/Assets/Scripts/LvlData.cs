﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LvlData
{
    [HideInInspector] public int[] LvlArr;
    [HideInInspector] public Vector3 CameraPos;
    [HideInInspector] public Quaternion CameraRotation;

    public int rows;
    public int PlayerSize;



    public int[,] Get2DLvlArr()
    {
        int[,] lvlArr2D = new int[rows, (LvlArr.Length / rows)];
        int counter = 0;

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < (LvlArr.Length / rows); j++)
            {
                lvlArr2D[i, j] = LvlArr[counter];
                counter++;
            }
        }

        return lvlArr2D;
    }
}
