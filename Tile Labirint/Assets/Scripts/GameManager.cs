﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum GameState
{
    WaitForInput,
    PlayerMoving,
    Wait,
}
    


public class GameManager : MonoBehaviour
{
    [SerializeField] LvlController LvlController;
    [SerializeField] PlayerInput playerInput;
    [SerializeField] Player player;
    [SerializeField] ObjectPooler objectPooler;
    [SerializeField] UI ui;

    int nextTileIndex;
    int lvlIndex;

    private GameState currentGameState;

    
    void Start()
    {
        currentGameState = GameState.Wait;

        objectPooler = ObjectPooler.Instance;
        objectPooler.FillPool();

        lvlIndex = PlayerPrefs.GetInt("LvlIndex");
        if (lvlIndex == 0) lvlIndex = 1;
      
        LvlController.BuildLvl(objectPooler, lvlIndex);
        player.SetPlayer(LvlController.GetTileArr());

        currentGameState = GameState.WaitForInput;
    }

    
    void Update()
    {
        switch (currentGameState)
        {
            case GameState.WaitForInput:
                 GetInput(playerInput.CheckInput());
                break;

            case GameState.PlayerMoving:
                CheckMoving();
                break;

            case GameState.Wait:
                break;
            
        }
    }

    private void GetInput(Vector3 moveDir)
    {
        if (moveDir == Vector3.zero) return;

        nextTileIndex = LvlController.GetNextTileIndex(DefineMoveDir(moveDir));
        player.MovePlayer(moveDir, nextTileIndex);

        currentGameState = GameState.PlayerMoving;
    }

    private void CheckMoving()
    {
        if (player.IsMoving()) return;

        if (nextTileIndex == 3)
        {
            ui.OnButtonClick += OnButtonUIWin; 
            ui.TurnWinUIOn();

            currentGameState = GameState.Wait;

            return;
        }

        if (player.IsLastTile())
        {
            ui.OnButtonClick += OnButtonUILose;
            ui.TurnLoseUIOn();

            currentGameState = GameState.Wait;

            return;
        }

         currentGameState = GameState.WaitForInput;
    }

    private MoveDirection DefineMoveDir(Vector3 dir)
    {
        if (dir == Vector3.left)
        {
            return MoveDirection.Left;
        }

        if (dir == Vector3.right)
        {
            return MoveDirection.Right;
        }

        if (dir == Vector3.back)
        {
            return MoveDirection.Back;
        }

        if (dir == Vector3.forward)
        {
            return MoveDirection.Forward;
        }
        return 0;
    }

    private void BuilNextLvl()
    {
        nextTileIndex = 0;

        if (!LvlController.IsLevelExist(lvlIndex)) lvlIndex = 1;

        PlayerPrefs.SetInt("LvlIndex", lvlIndex);

        LvlController.BuildLvl(objectPooler, lvlIndex);
        player.SetPlayer(LvlController.GetTileArr());
    }

    private void OnButtonUIWin()
    {
        ui.OnButtonClick -= OnButtonUIWin; 

        lvlIndex++;
        BuilNextLvl();

        ui.TurnWinUIOff();

        currentGameState = GameState.WaitForInput;
    }

    private void OnButtonUILose()
    {
        ui.OnButtonClick -= OnButtonUILose;

        BuilNextLvl();

        ui.TurnLoseUIOff();

        currentGameState = GameState.WaitForInput;
    }

    
}